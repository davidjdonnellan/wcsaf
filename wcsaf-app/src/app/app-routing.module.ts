import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { InfoComponent } from './info/info.component';
import { AmenitiesComponent } from './amenities/amenities.component';
import { HomeComponent } from './home/home.component';
import { GallaryComponent } from './gallary/gallary.component';
import { TrainingComponent } from './training/training.component';
import { ContactComponent } from './contact/contact.component';

const routes: Routes = [
  {path:  "", pathMatch:  "full",redirectTo:  "home"},
  {path: "home", component: HomeComponent},
  {path: "info", component: InfoComponent},
  {path: "amenities", component: AmenitiesComponent},
  {path: "gallary", component: GallaryComponent},
  {path: "training", component: TrainingComponent},  
  {path: "contact", component: ContactComponent}  

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
